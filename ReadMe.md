# EmailRep Playbook
link to the service: [https://emailrep.io/](https://emailrep.io/)<br/>

### 🔴The code is not being maintained anymore. I don't know how it works with newer and updated Sentinel features.🔴


## EmailRep.ip database
[EmailRep](https://emailrep.io/) is a database that helps you identify malicious and suspicious e-mail addresses.

This repo contains an Azure Sentinel Playbook that can be used to query the above-mentioned database to enrich your e-mail entities with additional information.<br/>

## Design considerations

The connections in the Playbook are implemented by using Managed Identities. This connection type is still in 'Preview' but it is a convenient way to handle connections, so I decided to go this way. I'm going to explain later on how to change the connection type if you want to use something else.

The Playbook comes in two flavors. There is a Playbook with an 'Incident' trigger and one with 'Alert' trigger.

The following Entities are processed by the Playbook:
- Mail message: Recipient
- Mail message: Sender
- Mail message: P1Sender
- Mail message: P2Sender



The Playbook modifies an Incident in two ways:
1. A comment is added to the incident with the following information in it:
	- If the information gathering is successful you will see an evaluation (clean or suspicious), an explanation of the evaluation, and a table with detailed information. One comment per successful info collection.
	- Every unsuccessful information gathering creates 1 common comment with the reason of the failure in it. One comment for all of the failed collections.
1. A tag (label) is added to the incident in the following format per entity (e-mail address): {Playbook Name}:{email}=[Clean|Sucpicious]. This format mimics the taxonomies in TheHive. At this point in Azure Sentinel, it is not possible to tag individual entities, all of the tags are related to the incident itself, so I decided to use the following tagging format in my Playbooks (until a better option): {PlaybookName}:{Entity}={Conclusion}. See an example below:

**Successful query**:<br/>
![comment](../static/comment.PNG)

**Unsuccessful query**:<br/>
![comment](../static/un_comment.PNG)

**Tagging**:<br/>
![tagging](../static/tagging.PNG)

## Installation and Configuration

**Deployment**:
1. Open the template deployment portal in Azure (https://portal.azure.com/#create/Microsoft.Template).
1. Click on the "Build your own template in the editor" button.
1. Copy the chosen template (azuredeploy.json) into the text field.
1. Click save and provide the requested information:
	1. Subscription
	1. Resource group
	1. Region
	1. Playbook Name
	1. Key Vault Name: The Key Vault in which the API key is stored.
	1. Secret Name: Name of the secret that stores the Spur API key.
	1. UserAgent: EmailRep requires a custom user agent. (in theory)
1. Deploy the code.

**Configuration**:
1. After deployment the managed identity is turned on for the resource (Playbook) and the API Connections are created, but you have to give permissions to the Managed Identity.
1. Open the Playbook.
1. Click on the Identity button in the Settings section.
1. Click on the Azure Role Assignment button.
1. Provide the following roles to your Managed Identity:
	Key Vault Secret User to your key vault (only works if your Key Vault is configured to use Azure role-based access control)
	Azure Sentinel Responder
1. Wait a few minutes for the roles to propagate.
1. Everything should work fine now.

**Created Resources:**

1. {Plabook Name}: Logic App
1. sen-{Playbook Name}: API Connection - to connect to Sentinel
1. kva-{Playbook Name}: API Connection - to connect to the KeyVault

If you don't want to use Managed Identity, then after deployment you can modify every block with connection requirement to use another method. <br/>
The API connections are deployed to be used by Managed Identities, so if you don't utilize Managed Identity you can remove these API connections from the resource group. Also, if you don't want to use KeyVault, you can delete the second API connection <br/>





## Future:
- Further testing of the code.
